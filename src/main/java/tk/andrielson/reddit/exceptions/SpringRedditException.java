package tk.andrielson.reddit.exceptions;

import org.springframework.mail.MailException;

public class SpringRedditException extends RuntimeException {
    public SpringRedditException(String exMessage, MailException mailException) {
        super(exMessage, mailException);
    }

    public SpringRedditException(String message) {
        super(message);
    }
}
