package tk.andrielson.reddit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tk.andrielson.reddit.model.Post;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {
}
