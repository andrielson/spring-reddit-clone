package tk.andrielson.reddit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tk.andrielson.reddit.model.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
}
